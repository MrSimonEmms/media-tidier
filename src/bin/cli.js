#!/usr/bin/env node

/**
 * cli
 */

/* Node modules */
const path = require('path');

/* Third-party modules */
const yargs = require('yargs');

/* Files */
const logger = require('../lib/logger');
const tasks = require('../tasks');

function getAbsPath(relPath) {
  if (!path.isAbsolute(relPath)) {
    return path.join(process.cwd(), relPath);
  }

  return relPath;
}

module.exports = yargs
  .scriptName('media-tidy')
  .usage('$0 <cmd> [args]')
  .command({
    command: 'fix',
    describe: 'Fix media downloaded and sorted by programme and series',
    builder: () => yargs.options({
      p: {
        alias: 'path',
        demandOption: true,
        describe: 'Set the path to search from',
        type: 'string',
      },
      i: {
        alias: 'ignore',
        describe: 'Ignored path',
        default: [],
        type: 'array',
      },
    }),
    async handler({ ignore, path: target }) {
      const targetDirectory = getAbsPath(target);
      const ignorePaths = ignore.map((item) => getAbsPath(item));

      const log = logger.child({
        path: targetDirectory,
        task: 'fix',
      });

      log.debug({ ignorePaths }, 'Ignored paths');

      try {
        await tasks.fix(log, targetDirectory, ignorePaths);
      } catch (err) {
        log.fatal({ err }, 'Uncaught exception');

        process.exit(1);
      }
    },
  })
  .alias('v', 'version')
  .alias('h', 'help')
  .demandCommand()
  .recommendCommands()
  .help()
  .parse();
