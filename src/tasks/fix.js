/**
 * fix
 */

/* Node modules */

/* Third-party modules */
const glob = require('glob');
const NodeId3 = require('node-id3');

/* Files */
const extractMetaData = require('../lib/extractMetaData');

module.exports = async (log, filePath, ignore) => {
  log.info('Starting');

  const files = glob.sync(`${filePath}/**/*`, {
    ignore: ignore.map((item) => `${item}/**/*`),
    nodir: true,
    nosort: true,
  });

  await Promise.all(files.map(async (file) => {
    const tags = extractMetaData(log, filePath, file);

    log.debug({ file }, 'Writing tags');

    NodeId3.update({
      artist: tags.artist,
      album: tags.album,
      performerInfo: tags.artist,
      title: tags.title,
    }, file);

    log.info({ file, tags }, 'Tags written');
  }));

  log.info({
    count: files.length,
  }, 'Finished');
};
