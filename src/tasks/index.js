/**
 * index
 */

/* Node modules */

/* Third-party modules */

/* Files */
const fix = require('./fix');

module.exports = {
  fix,
};
