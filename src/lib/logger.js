/**
 * logger
 */

/* Node modules */

/* Third-party modules */
const pino = require('pino');

/* Files */

module.exports = pino({
  name: 'media-tidy',
  level: process.env.LOG_LEVEL || 'info',
});
