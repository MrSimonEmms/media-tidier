/**
 * extractMetaData
 */

/* Node modules */
const path = require('path');

/* Third-party modules */

/* Files */

module.exports = function extractMetaData(log, filePath, file) {
  const re = new RegExp(`^${filePath}(/)?`);
  const fileStr = file.replace(re, '');
  const [artist = '', ...fileParts] = fileStr.split(path.sep);

  let album;
  if (fileParts.length === 1) {
    /* No album directory - default to Series 1 */
    album = 'Series 1';
  } else {
    album = fileParts[0] || '';
  }

  const { name = '' } = path.parse(fileStr);

  const title = name
    .replace(/^(\d+\s?\.)/, '') // number<dot>
    .replace(/^((\d+)\s?-\s?([\w\s]+\s-\s)?)/, '') // number<dash>
    .replace(/^([\w\s'"]+-\s[se\d]+\s-)/, '') // show <dash> number <dash> name
    .trim();

  log.debug({
    file,
    artist,
    album,
    title,
  }, 'Extracting meta data for file');

  return {
    artist,
    album,
    title,
  };
};
