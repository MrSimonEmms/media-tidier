/**
 * setup
 */

/* Node modules */

/* Third-party modules */
const chai = require('chai');
const { noCallThru } = require('proxyquire');
const sinon = require('sinon');
const sinonChai = require('sinon-chai');

/* Files */

chai.use(sinonChai);

module.exports = {
  sinon,
  expect: chai.expect,
  proxyquire: noCallThru(),
};
