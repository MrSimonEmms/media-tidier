/**
 * extractMetaData.test
 */

/* Node modules */
const path = require('path');

/* Third-party modules */

/* Files */
const extractMetaData = require('../../../src/lib/extractMetaData');
const { expect, sinon } = require('../../helpers/setup');

function generateFilePath(artist, album, title) {
  const filePath = process.cwd();

  const args = [
    artist,
    album,
    title,
  ].filter((item) => !!item);

  return {
    filePath,
    file: path.join(filePath, ...args),
  };
}

describe('fix task test', () => {

  beforeEach(() => {
    this.log = {
      debug: sinon.spy(),
    };
  });

  describe('#extractMetaData', () => {

    it('should extract the meta data for a simple name', () => {
      const artist = 'Welcome to Our Village Please Invade Carefully';
      const album = 'Series 1';
      const title = 'Power Block';
      const { filePath, file } = generateFilePath(artist, album, `${title}.mp3`);

      expect(extractMetaData(this.log, filePath, file)).to.be.eql({
        artist,
        album,
        title,
      });
    });

    it('should extract the meta data for a "number<dot>" name', () => {
      const artist = 'Welcome to Our Village Please Invade Carefully';
      const album = 'Series 1';
      const title = 'Power Block';
      const { filePath, file } = generateFilePath(artist, album, `3. ${title}.mp3`);

      expect(extractMetaData(this.log, filePath, file)).to.be.eql({
        artist,
        album,
        title,
      });
    });

    it('should extract the meta data for a "number <dash> " name', () => {
      const artist = 'Welcome to Our Village Please Invade Carefully';
      const album = 'Series 1';
      const title = 'Power Block';
      const { filePath, file } = generateFilePath(artist, album, `3 - ${title}.mp3`);

      expect(extractMetaData(this.log, filePath, file)).to.be.eql({
        artist,
        album,
        title,
      });
    });

    it('should extract the meta data for a "show <dash> number <dash> " name', () => {
      const artist = 'Welcome to Our Village Please Invade Carefully';
      const album = 'Series 1';
      const title = 'Power Block';
      const { filePath, file } = generateFilePath(artist, album, `${artist} - 103 - ${title}.mp3`);

      expect(extractMetaData(this.log, filePath, file)).to.be.eql({
        artist,
        album,
        title,
      });
    });

    it('should extract the meta data for a "show <dash> s<number>e<number> <dash> "', () => {
      const artist = 'Welcome to Our Village Please Invade Carefully';
      const album = 'Series 1';
      const title = 'Power Block';
      const { filePath, file } = generateFilePath(artist, album, `${artist} - s01e03 - ${title}.mp3`);

      expect(extractMetaData(this.log, filePath, file)).to.be.eql({
        artist,
        album,
        title,
      });
    });

    it('should extract the meta data when a hyphen in the file name', () => {
      const artist = 'Chain Reaction';
      const album = 'Series 11';
      const title = 'Victoria Coren-Mitchell interviews Sandi Toksvig';
      const { filePath, file } = generateFilePath(artist, album, `${title}.ogg`);

      expect(extractMetaData(this.log, filePath, file)).to.be.eql({
        artist,
        album,
        title,
      });
    });

    it('should extract the meta data when a hyphen in the file name with "number<dot> "', () => {
      const artist = 'Chain Reaction';
      const album = 'Series 11';
      const title = 'Victoria Coren-Mitchell interviews Sandi Toksvig';
      const { filePath, file } = generateFilePath(artist, album, `1. ${title}.ogg`);

      expect(extractMetaData(this.log, filePath, file)).to.be.eql({
        artist,
        album,
        title,
      });
    });

    it('should extract the meta data when a hyphen in the file name with "number <dash> "', () => {
      const artist = 'Chain Reaction';
      const album = 'Series 11';
      const title = 'Victoria Coren-Mitchell interviews Sandi Toksvig';
      const { filePath, file } = generateFilePath(artist, album, `1 - ${title}.ogg`);

      expect(extractMetaData(this.log, filePath, file)).to.be.eql({
        artist,
        album,
        title,
      });
    });

    it('should extract the meta data when a hyphen in the file name with "show <dash> <number> <dash> "', () => {
      const artist = 'Chain Reaction';
      const album = 'Series 11';
      const title = 'Victoria Coren-Mitchell interviews Sandi Toksvig';
      const { filePath, file } = generateFilePath(artist, album, `${artist} - 1101 - ${title}.ogg`);

      expect(extractMetaData(this.log, filePath, file)).to.be.eql({
        artist,
        album,
        title,
      });
    });

    it('should extract the meta data when a hyphen in the file name with "show <dash> s<number>e<number> <dash> "', () => {
      const artist = 'Chain Reaction';
      const album = 'Series 11';
      const title = 'Victoria Coren-Mitchell interviews Sandi Toksvig';
      const { filePath, file } = generateFilePath(artist, album, `${artist} - s11e01 - ${title}.ogg`);

      expect(extractMetaData(this.log, filePath, file)).to.be.eql({
        artist,
        album,
        title,
      });
    });

    it('should extract the meta data when a quote in the file name with "number<dot> "', () => {
      const artist = 'Old Harry\'"s Game';
      const album = 'Series 01';
      const title = 'Welcome to Hell';
      const { filePath, file } = generateFilePath(artist, album, `1. ${title}.ogg`);

      expect(extractMetaData(this.log, filePath, file)).to.be.eql({
        artist,
        album,
        title,
      });
    });

    it('should extract the meta data when a quote in the file name with "number <dash> "', () => {
      const artist = 'Old Harry\'"s Game';
      const album = 'Series 01';
      const title = 'Welcome to Hell';
      const { filePath, file } = generateFilePath(artist, album, `1 - ${title}.ogg`);

      expect(extractMetaData(this.log, filePath, file)).to.be.eql({
        artist,
        album,
        title,
      });
    });

    it('should extract the meta data when a quote in the file name with "show <dash> <number> <dash> "', () => {
      const artist = 'Old Harry\'"s Game';
      const album = 'Series 01';
      const title = 'Welcome to Hell';
      const { filePath, file } = generateFilePath(artist, album, `${artist} - 1101 - ${title}.ogg`);

      expect(extractMetaData(this.log, filePath, file)).to.be.eql({
        artist,
        album,
        title,
      });
    });

    it('should extract the meta data when a hyphen in the file name with "show <dash> s<number>e<number> <dash> "', () => {
      const artist = 'Old Harry\'"s Game';
      const album = 'Series 01';
      const title = 'Welcome to Hell';
      const { filePath, file } = generateFilePath(artist, album, `${artist} - s11e01 - ${title}.ogg`);

      expect(extractMetaData(this.log, filePath, file)).to.be.eql({
        artist,
        album,
        title,
      });
    });

    it('should extract the meta data when a hyphen in the file name with "number <dash> show <dash>"', () => {
      const artist = 'Behaving Ourselves Mitchell on Manners';
      const album = 'Series 11';
      const title = 'A Bit of History';
      const { filePath, file } = generateFilePath(artist, album, `01 - ${artist} - ${title}.mp3`);

      expect(extractMetaData(this.log, filePath, file)).to.be.eql({
        artist,
        album,
        title,
      });
    });

    it('should extract the meta data when a quote in the file name with "number <dash> show <dash>"', () => {
      const artist = 'Old Harry\'"s Game';
      const album = 'Series 01';
      const title = 'Welcome to Hell';
      const { filePath, file } = generateFilePath(artist, album, `${artist} - 01 - ${title}.mp3`);

      expect(extractMetaData(this.log, filePath, file)).to.be.eql({
        artist,
        album,
        title,
      });
    });

    it('should extract the meta data when no series folder', () => {
      const artist = 'A Beginner\'s Guide to India';
      const album = null;
      const title = 'Women';
      const { filePath, file } = generateFilePath(artist, album, `1. ${title}.mp3`);

      expect(extractMetaData(this.log, filePath, file)).to.be.eql({
        artist,
        title,
        album: 'Series 1',
      });
    });

  });

});
